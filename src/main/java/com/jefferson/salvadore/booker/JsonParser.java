/*
 * Copyright © 2016 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jefferson.salvadore.booker.publication.Book;
import com.jefferson.salvadore.booker.publication.PublicationFrequency;
import com.jefferson.salvadore.booker.publication.Range;


/**
 * A helper class for parsing JSON data from an Internet resource and creating a corresponding
 * {@Link Book}.
 * 
 * @author Salvadore Jefferson
 * @version 2.0.0 3-09-16
 *
 */
public class JsonParser {

  /**
   * Creates an {@link InputStream} from the provided {@link Path} and delegates the parsing to
   * {@link #parseJson(InputStream)} with the newly created <code>InputStream</code> as the argument
   * to the method call.
   * 
   * @param path The <code>Path</code> to the JSON file.
   * @return book The {@link Book} representation contained in the JSON file.
   * @throws IOException Thrown if there is an issue accessing the file.
   */
  public static Book parseJson(@Nonnull final Path path) throws IOException {
    try (final InputStream source = Files.newInputStream(path)) {
      final Book book = parseJson(source);
      return book;
    }
  }

  /**
   * Parses JSON objects from an InputStream and creates an {@link Book} object from the results.
   * 
   * @param source The InputStream to parse
   * @return book The {@link Book} representation contained in the JSON file.
   * @throws JsonParseException
   * @throws JsonMappingException
   * @throws IOException
   */
  public static Book parseJson(@Nonnull final InputStream source)
      throws JsonParseException, JsonMappingException, IOException {
    final JsonNode rootNode = getRootNode(source);
    final JsonNode volumeInfo = rootNode.findValue("volumeInfo");
    final JsonNode authors = volumeInfo.path("authors");

    final String author = authors.get(0).textValue();
    final String title = volumeInfo.path("title").textValue();
    final String year = (String) volumeInfo.path("publishedDate").textValue().subSequence(0, 4);
    final int yearPublished = Integer.parseInt(year);

    final Book book = new Book(title, author, Range.of(yearPublished), PublicationFrequency.NA);
    return book;
  }

  /**
   * Gets the root node of a JSON object from an {@link InputStream}
   * 
   * @param source The <code>InputStream</code> of the JSON object
   * @return rootNode The root node of the resultant JSON object
   * @throws IOException Thrown if an error occurs while reading from the stream
   */
  public static JsonNode getRootNode(@Nonnull final InputStream source) throws IOException {
    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode rootNode = mapper.readTree(source);
    return rootNode;
  }

  /**
   * Creates an {@link InputStream} from a {@link Path} and delegates the generation of the root
   * node to {@link #getRootNode(InputStream)}, with the newly created<code>InputStream</code> as
   * the argument to the method call
   * 
   * @param path The <code>Path</code> to a JSON file
   * @return rootNode The root node for the resultant JSON object
   * @throws IOException Thrown if an error occurs while accessing the file system
   */
  public static JsonNode getRootNode(@Nonnull final Path path) throws IOException {
    try (final InputStream source = Files.newInputStream(path)) {
      final JsonNode node = getRootNode(source);
      return node;
    }
  }
}
