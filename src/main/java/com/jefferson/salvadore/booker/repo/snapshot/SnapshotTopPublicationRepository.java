/*
 * Copyright © 2015 Salvadore Jefferson
 */

package com.jefferson.salvadore.booker.repo.snapshot;

import static com.jefferson.salvadore.booker.publication.PublicationFrequency.MONTHLY;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.NA;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.UNDISCLOSED;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.WEEKLY;
import static com.jefferson.salvadore.booker.publication.Range.of;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.jefferson.salvadore.booker.publication.Book;
import com.jefferson.salvadore.booker.publication.Journal;
import com.jefferson.salvadore.booker.publication.Magazine;
import com.jefferson.salvadore.booker.publication.Publication;
import com.jefferson.salvadore.booker.repo.PublicationRepository;

/**
 * A snapshot of the best-selling publications according to Wikipedia on 08-28-2015
 * 
 * @version 2.0.0 4-3-16
 * @author Salvadore Jefferson
 */
public class SnapshotTopPublicationRepository implements PublicationRepository {

  private static final Integer DEFAULT_INVENTORY_COUNT = Integer.valueOf(0);
  private final ImmutableList<Publication> immutablePublicationList;
  private final Map<String, Integer> publicationMap;

  public SnapshotTopPublicationRepository() {
    final ImmutableList.Builder<Publication> listBuilder = ImmutableList.builder();

    listBuilder.add(new Book("A Tale of Two Cities", "Charles Dickens", of(0), 200_000_000, NA));
    listBuilder
        .add(new Book("The Lord of The Rings", "J.R.R Tolkien", of(1954, 1955), 150_000_000, NA));
    listBuilder.add(new Book("The Hobbit", "J.R.R Tolkien", of(1937), 140_600_000, NA));
    listBuilder.add(new Book("Harry Potter and the Philosopher's Stone", "J. K. Rowling", of(1997),
        107_000_000, NA));
    listBuilder.add(new Book("Le Petit Prince", "Antoine de Saint-Exupéry", "French", of(1943),
        140_000_000, NA));
    listBuilder.add(new Book("紅樓夢/红楼梦 (Dream of the Red Chamber)", "Cao Xueqin", of(1754, 1791),
        100_000_000, NA));
    listBuilder.add(new Book("And Then There Were None", "Agatha Christie", of(1939), NA));
    listBuilder.add(new Book("She: A History of Adventure", "H. Rider Haggard", of(1887), NA));
    listBuilder.add(new Magazine("AARP The Magazine", "AARP", of(1958), 22_274_096, MONTHLY));
    listBuilder.add(new Magazine("AARP Bulletin", "AARP", of(1960), 22_244_820, MONTHLY));
    listBuilder
        .add(new Magazine("Costco Connection", "Costco Wholesale", of(0), 8_654_464, MONTHLY));
    listBuilder
        .add(new Magazine("Better Homes And Gardens", "Meredith", of(1922), 7_615_581, MONTHLY));
    listBuilder.add(new Magazine("Game Informer", "GameStop", of(1991), 7_629_995, MONTHLY));
    listBuilder.add(new Journal("PLOS ONE", "Public Library of Science", of(2006), UNDISCLOSED,
        "Multidisciplinary", 3.234));
    listBuilder.add(new Journal("Philosophical Transactions of the Royal Society", "Royal Society",
        of(1665), UNDISCLOSED, "Physical Sciences", 7.055));
    listBuilder.add(new Journal("Nature Communications", "Nature Publishing Group", of(2010),
        UNDISCLOSED, "Earth Sciences", 11.47));
    listBuilder.add(new Journal("Nature", "Nature Publishing Group", of(1869), WEEKLY,
        "Natural Science", 41.456));
    listBuilder.add(new Journal(
        "Proceedings of the National Academy of Sciences of the United States of America",
        "United States National Academy of Sciences", of(1914), WEEKLY, "Multidisciplinary",
        9.674));

    immutablePublicationList = listBuilder.build();
    publicationMap = buildPublicationMap(immutablePublicationList);
  }

  @Override
  public void addPub(@Nonnull final Publication publication) throws IOException {
    throw new UnsupportedOperationException("Cannot add to this repository");
  }

  @Override
  public void removePubByTitle(@Nonnull final String title) throws IOException {
    throw new UnsupportedOperationException("Cannot remove from this repository");
  }

  @Override
  public Publication findByTitle(@Nonnull final String title) throws IOException {
    for (int i = 0; i < immutablePublicationList.size(); i++) {
      final Publication publication = immutablePublicationList.get(i);
      if (publication.getTitle().equals(title)) {
        return publication;
      }
    }
    throw new NoSuchElementException("this title does not exist: " + title);
  }

  @Override
  public <P extends Publication> List<P> getByType(@Nonnull final Class<P> publicationType)
      throws IOException {
    final List<P> theList = new ArrayList<P>();
    for (int i = 0; i < immutablePublicationList.size(); i++) {
      final Publication publication = immutablePublicationList.get(i);
      if (publicationType.isInstance(publication)) {
        theList.add(publicationType.cast(publication));
      }
    }
    return theList;
  }

  @Override
  public void printAllPubs() {
    for (Publication pub : immutablePublicationList) {
      System.out.println(pub);
    }
  }

  @Override
  public void setInventoryCount(@Nonnull final String title, int count) {
    publicationMap.replace(title, count);
  }

  @Override
  public Integer getInventoryCount(@Nonnull final String title) {
    return publicationMap.getOrDefault(title, DEFAULT_INVENTORY_COUNT);
  }

  /**
   * Builds a {@link HashMap} based on the contents of the local {@link List} of
   * {@link Publications}. The <code>Publications</code> title is used for the <code>key</code> and
   * the inventory count is set to zero by default.
   * 
   */
  public static HashMap<String, Integer> buildPublicationMap(
      @Nonnull final List<Publication> pubList) {
    HashMap<String, Integer> pubMap = new HashMap<>();
    for (Publication pub : pubList) {
      pubMap.put(pub.getTitle(), DEFAULT_INVENTORY_COUNT);
    }
    return pubMap;
  }

  @Override
  public int getRepoSize() {
    return immutablePublicationList.size();
  }
}
