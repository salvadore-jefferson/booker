/*
 * Copyright © 2015 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker.repo;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.jefferson.salvadore.booker.publication.Publication;
import com.jefferson.salvadore.booker.repo.file.FilePublicationRepository;

/**
 * An implementation of {@link PublicationManager}, for manipulating the inventory level of the
 * {@link Publication}s in a {@link FilePublicationRepository}
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 12-02-15
 *
 */
@Singleton
public class PublicationManagerImpl implements PublicationManager {

  private PublicationRepository repo;


  @Inject
  @Override
  public void setRepo(@Nonnull final PublicationRepository repo) {
    this.repo = checkNotNull(repo);
  }

  @Override
  public void addStockToInventory(@Nonnull final String title, @Nonnull final Integer amountToAdd)
      throws IOException {
    final Integer newCount = repo.getInventoryCount(title) + amountToAdd;
    repo.setInventoryCount(title, newCount);

  }

  @Override
  public void removeStockFromInventory(@Nonnull final String title,
      @Nonnull final Integer amountToRemove) throws IOException {
    final Integer newCount = repo.getInventoryCount(title) - amountToRemove;
    repo.setInventoryCount(title, newCount);

  }

  @Override
  public Integer getInventoryCount(@Nonnull final String title) throws IOException {
    return repo.getInventoryCount(title);
  }

}
