/*
 * Copyright © 2016 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker.publication;

import javax.annotation.Nonnull;

import com.jefferson.salvadore.booker.Booker;

/**
 * A set of type <code>Enum</code> used to represent the publication <code>Type</code> of
 * {@link Publication} in the {@link Booker} application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 2-07-2016
 *
 */
public enum PublicationType {

  /**
   * A Book
   */
  BOOK("Book"),

  /**
   * A Journal
   */
  JOURNAL("Journal"),

  /**
   * An Magazine
   */
  MAGAZINE("Magazine");

  private final String value;

  private PublicationType(final String value) {
    this.value = value;
  }

  /**
   * A static factory method for assigning a <code>Type</code> to a {@link Publication}
   * 
   * @param type
   * @return Type
   */
  public static PublicationType of(@Nonnull final PublicationType type) {
    return type;
  }

  /**
   * Returns a <code>String</code> representing the publication type.
   */
  public String getValue() {
    return value;
  }
}
