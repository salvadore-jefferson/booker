/*
 * Copyright © 2015 Salvadore Jefferson
 */

package com.jefferson.salvadore.booker.publication;

import com.jefferson.salvadore.booker.Booker;

/**
 * A set of type <code>Enum</code> used to represent the publication frequency of
 * {@link Publication} in the {@link Booker} application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 10-25-2015
 *
 */
public enum PublicationFrequency {
  /**
   * A weekly publication frequency
   */
  WEEKLY(52),

  /**
   * A Monthly publication frequency
   */
  MONTHLY(12),

  /**
   * An unknown publication frequency
   */
  UNDISCLOSED(0),

  /**
   * An <code>Enum</code> for non-applicable publication types. For example {@link Book}
   */
  NA(-1);

  private final int freqValue;

  private PublicationFrequency(final int freqValue) {
    this.freqValue = freqValue;
  }

  /**
   * Returns an <code>int</code> representing the number of issues of this <code>Publication</code>
   * per year. Zero is returned to indicate an unknown <code>PublicationFrequency</code>, and -1 is
   * returned to indicate <code>frequency</code> is not applicable for this type of
   * {@link Publication}.
   * 
   * @see #UNDISCOLOSED
   * @see #NA
   */
  public int getFrequencyValue() {
    return freqValue;
  }
}
