/*
 * Copyright © 2015 Salvadore Jefferson
 */

package com.jefferson.salvadore.booker.publication;

import java.io.Serializable;
import java.util.Comparator;

/**
 * <p>
 * An interface used to define common behavior of multiple publication types.
 * </p>
 * 
 * @version 2.0.0 11-18-2015
 * @author Salvadore Jefferson
 */
public interface Publication extends Serializable {

  /**
   * Get the title of the publication.
   * 
   * @return String
   */
  String getTitle();

  /**
   * Get the range; in years, of the publication's publish date
   * 
   * @return Range
   */
  Range getYearsPublished();

  /**
   * Returns the instance of {@code PublicationFrequency} associated with the {@link Publication}
   * that calls the method.
   * 
   * @return The {@code PublicationFrequency} of this {@code Publication}
   */
  PublicationFrequency getPublicationFrequency();

  public static final Comparator<Publication> PUBFREQ_COMPARATOR = new Comparator<Publication>() {

    @Override
    public int compare(Publication o1, Publication o2) {
      return Integer.compare(o1.getPublicationFrequency().getFrequencyValue(),
          o2.getPublicationFrequency().getFrequencyValue());
    }
  };
}

