/*
 * Copyright © 2015 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker;

import com.google.inject.AbstractModule;
import com.jefferson.salvadore.booker.repo.PublicationManager;
import com.jefferson.salvadore.booker.repo.PublicationManagerImpl;
import com.jefferson.salvadore.booker.repo.PublicationRepository;
import com.jefferson.salvadore.booker.repo.snapshot.SnapshotTopPublicationRepository;

/**
 * A utility class to inject dependencies needed to execute the {@link Booker} application
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 12-05-15
 *
 */
public class BookerModule extends AbstractModule {

  @Override
  protected void configure() {
    // uncomment this code when a FilePublicationRepository is needed
    /*-   try {
      bind(PublicationRepository.class)
          .toInstance(new FilePublicationRepository(CONFIG_DIR.resolve("publication-files")));
    } catch (IOException e) {
      e.printStackTrace();
    }
    */
    bind(PublicationRepository.class).to(SnapshotTopPublicationRepository.class);
    bind(BookerServices.class).to(BookerServicesImpl.class);
    bind(PublicationManager.class).to(PublicationManagerImpl.class);
  }
}
