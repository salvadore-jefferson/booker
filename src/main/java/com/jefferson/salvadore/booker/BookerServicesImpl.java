/*
 * Copyright © 2015 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jefferson.salvadore.booker.publication.PublicationType.BOOK;
import static com.jefferson.salvadore.booker.publication.PublicationType.JOURNAL;
import static com.jefferson.salvadore.booker.publication.PublicationType.MAGAZINE;
import static java.net.HttpURLConnection.HTTP_OK;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jefferson.salvadore.booker.publication.Book;
import com.jefferson.salvadore.booker.publication.Journal;
import com.jefferson.salvadore.booker.publication.Magazine;
import com.jefferson.salvadore.booker.publication.PublicationType;
import com.jefferson.salvadore.booker.repo.PublicationManager;
import com.jefferson.salvadore.booker.repo.PublicationRepository;

/**
 * An implementation of the {@link BookerServices} interface.
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 12-02-15
 *
 */
@Singleton
public class BookerServicesImpl implements BookerServices {
  static final Logger logger = LoggerFactory.getLogger(BookerServicesImpl.class);
  public static final URI GOOGLE_BOOKS_API = URI.create("https://www.googleapis.com/books/v1/");

  private final PublicationManager manager;

  @Inject
  public BookerServicesImpl(@Nonnull final PublicationManager manager) {
    this.manager = checkNotNull(manager);
  }

  @Override
  public void purchasePublication(@Nonnull final String title, @Nonnull final Integer quantity)
      throws IOException {

    manager.removeStockFromInventory(title, quantity);
  }

  @Override
  public Integer checkInventory(@Nonnull final String title) throws IOException {
    return manager.getInventoryCount(title);
  }

  @Override
  public void printPubByType(@Nonnull final PublicationRepository repo,
      @Nonnull final Set<PublicationType> pubTypes) throws IOException {
    for (final PublicationType pubType : pubTypes) {
      if (pubType.equals(BOOK)) {
        final List<Book> books = repo.getByType(Book.class);
        for (final Book book : books) {
          System.out.println(book.toString());
        }
      }
      if (pubType.equals(MAGAZINE)) {
        final List<Magazine> mags = repo.getByType(Magazine.class);
        for (final Magazine mag : mags) {
          System.out.println(mag.toString());
        }
      }
      if (pubType.equals(JOURNAL)) {
        final List<Journal> journals = repo.getByType(Journal.class);
        for (final Journal journal : journals) {
          System.out.println(journal.toString());
        }
      }
    }
  }

  @Override
  public void subscribeToPeriodical(@Nonnull final PublicationRepository repo,
      @Nonnull final String title, final int issueCount) throws IOException {
    final SubscriptionService subscriber = new SubscriptionService(repo, title, issueCount);
    new Thread(subscriber).start();
  }

  @Override
  public Book lookupBookByIsbn(@Nonnull final String isbn)
      throws MalformedURLException, IOException {
    final String urlResolver = "volumes?q=isbn:" + isbn;
    final URI resolvedURI = GOOGLE_BOOKS_API.resolve(urlResolver);
    final HttpURLConnection connection = (HttpURLConnection) resolvedURI.toURL().openConnection();
    connection.setRequestMethod("GET");
    if (connection.getResponseCode() != HTTP_OK) {
      logger.info("an error occured while establishing this connection");
      throw new IOException();
    }
    try (final InputStream inputStream = new BufferedInputStream(connection.getInputStream())) {
      final Book book = JsonParser.parseJson(inputStream);
      return book;
    }
  }
}
