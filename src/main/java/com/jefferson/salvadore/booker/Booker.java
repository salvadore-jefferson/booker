// Copyright © 2015 Salvadore Jefferson

package com.jefferson.salvadore.booker;

import static com.jefferson.salvadore.booker.publication.PublicationFrequency.MONTHLY;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.NA;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.UNDISCLOSED;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.WEEKLY;
import static com.jefferson.salvadore.booker.publication.Range.of;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.Set;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jefferson.salvadore.booker.publication.Book;
import com.jefferson.salvadore.booker.publication.Journal;
import com.jefferson.salvadore.booker.publication.Magazine;
import com.jefferson.salvadore.booker.publication.Publication;
import com.jefferson.salvadore.booker.publication.PublicationType;
import com.jefferson.salvadore.booker.repo.file.FilePublicationRepository;

/**
 * <p>
 * The <code>Booker Application</code> utilizes the {@link PublicationRepositiry} interface to
 * perform actions on a data store of {@link Publication} objects.
 * </p>
 * 
 * @version 44.0.0 1-05-2016
 * 
 * @author Salvadore Jefferson
 * 
 */
public class Booker {
  private static final ImmutableList<Publication> IMMUTABLE_REPO;

  static {
    final ImmutableList.Builder<Publication> listBuilder = ImmutableList.builder();

    listBuilder.add(new Book("A Tale of Two Cities", "Charles Dickens", of(0), 200_000_000, NA));
    listBuilder
        .add(new Book("The Lord of The Rings", "J.R.R Tolkien", of(1954, 1955), 150_000_000, NA));
    listBuilder.add(new Book("The Hobbit", "J.R.R Tolkien", of(1937), 140_600_000, NA));
    listBuilder.add(new Book("Harry Potter and the Philosopher's Stone", "J. K. Rowling", of(1997),
        107_000_000, NA));
    listBuilder.add(new Book("Le Petit Prince", "Antoine de Saint-Exupéry", "French", of(1943),
        140_000_000, NA));
    listBuilder.add(new Book("紅樓夢/红楼梦 (Dream of the Red Chamber)", "Cao Xueqin", of(1754, 1791),
        100_000_000, NA));
    listBuilder.add(new Book("And Then There Were None", "Agatha Christie", of(1939), NA));
    listBuilder.add(new Book("She: A History of Adventure", "H. Rider Haggard", of(1887), NA));
    listBuilder.add(new Magazine("AARP The Magazine", "AARP", of(1958), 22_274_096, MONTHLY));
    listBuilder.add(new Magazine("AARP Bulletin", "AARP", of(1960), 22_244_820, MONTHLY));
    listBuilder
        .add(new Magazine("Costco Connection", "Costco Wholesale", of(0), 8_654_464, MONTHLY));
    listBuilder
        .add(new Magazine("Better Homes And Gardens", "Meredith", of(1922), 7_615_581, MONTHLY));
    listBuilder.add(new Magazine("Game Informer", "GameStop", of(1991), 7_629_995, MONTHLY));
    listBuilder.add(new Journal("PLOS ONE", "Public Library of Science", of(2006), UNDISCLOSED,
        "Multidisciplinary", 3.234));
    listBuilder.add(new Journal("Philosophical Transactions of the Royal Society", "Royal Society",
        of(1665), UNDISCLOSED, "Physical Sciences", 7.055));
    listBuilder.add(new Journal("Nature Communications", "Nature Publishing Group", of(2010),
        UNDISCLOSED, "Earth Sciences", 11.47));
    listBuilder.add(new Journal("Nature", "Nature Publishing Group", of(1869), WEEKLY,
        "Natural Science", 41.456));
    listBuilder.add(new Journal(
        "Proceedings of the National Academy of Sciences of the United States of America",
        "United States National Academy of Sciences", of(1914), WEEKLY, "Multidisciplinary",
        9.674));

    IMMUTABLE_REPO = listBuilder.build();
  }

  /**
   * <code>CONFIG_DIR</code> is the path to the .booker directory located in the users root
   * directory. This <code>Path</code> is to be used when working with
   * {@link FilePublicationRepository}
   */
  public static final Path CONFIG_DIR = Paths.get(System.getProperty("user.home"), ".booker");

  public static String LIST = "list";
  public static String PURCHASE = "purchase";
  public static String SUBSCRIBE = "subscribe";
  public static String COMMAND = "command";

  public static int issueCount;
  public static boolean help;
  public static Set<PublicationType> pubTypes;
  public static boolean verbose;
  public static boolean debug;
  public static boolean lookUp;
  public static boolean info;
  public static String pubTypesString;
  public static String title;
  public static String isbn;

  static final Logger logger = LoggerFactory.getLogger(Booker.class);

  public static void main(String[] args) {

    // Print list of Publications
    printAll();
    final Logger rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    ((ch.qos.logback.classic.Logger) rootLogger).setLevel(ch.qos.logback.classic.Level.WARN);

    final CommandLineOptions commandLineOpts = new CommandLineOptions();
    final CmdLineParser parser = new CmdLineParser(commandLineOpts);

    final Injector injector = Guice.createInjector(new BookerModule());
    final BookerServices service = injector.getInstance(BookerServices.class);
    // final PublicationRepository REPO =
    // injector.getInstance(SnapshotTopPublicationRepository.class);

    /*-try {
      parser.parseArgument(args);
      COMMAND = commandLineOpts.getCommand();
      help = commandLineOpts.help();
      verbose = commandLineOpts.verbose();
      debug = commandLineOpts.debug();
      pubTypesString = commandLineOpts.pubTypesString();
      title = commandLineOpts.getPubTitle();
      info = commandLineOpts.info();
      lookUp = commandLineOpts.lookUp();
      isbn = commandLineOpts.getIsbn();
    
      if (args.length <= 0 || help) {
        parser.printUsage(System.out);
      }
      if (debug) {
        ((ch.qos.logback.classic.Logger) rootLogger).setLevel(ch.qos.logback.classic.Level.DEBUG);
        commandLineOpts.verbose = false;
        logger.info("The current log level is set to DEBUG mode");
      }
      if (verbose) {
        ((ch.qos.logback.classic.Logger) rootLogger).setLevel(ch.qos.logback.classic.Level.INFO);
        logger.info("You have selected --verbose mode");
      }
      if (COMMAND != null && !COMMAND.equalsIgnoreCase(LIST) && !COMMAND.equalsIgnoreCase(PURCHASE)
          && !COMMAND.equalsIgnoreCase(SUBSCRIBE) && COMMAND != commandLineOpts.getCommand()) {
        logger.info("The argument you passed to the command line is not valid: {} ", COMMAND);
        parser.printUsage(System.out);
      }
      if (COMMAND.equalsIgnoreCase(LIST)) {
        if (pubTypesString != null) {
          try {
            pubTypes = commandLineOpts.getPubTypes();
            service.printPubByType(REPO, pubTypes);
          } catch (IOException e) {
            logger.error("An error occured will accessing a repository", e);
          }
        } else {
          logger.info("You have asked for all available publications");
          REPO.printAllPubs();
        }
      }
      if (COMMAND.equalsIgnoreCase(PURCHASE)) {
        try {
          service.purchasePublication(title, 1);
          logger.info("You just purchased a copy of: {} ", title);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    if (COMMAND.equalsIgnoreCase(SUBSCRIBE)) {
        try {
          issueCount = commandLineOpts.issueCount();
          service.subscribeToPeriodical(REPO, title, issueCount);
          logger.info("You have just subscribed to: {} ", title);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      if (info) {
        if (!lookUp) {
          logger.info("You must provide an additional argument for this command: <--lookup>");
        } else {
          try {
            logger.info("you have searched for {}", isbn);
            System.out.println(service.lookupBookByIsbn(isbn));
          } catch (MalformedURLException e1) {
            e1.printStackTrace();
          } catch (IOException e1) {
            e1.printStackTrace();
          }
        }
      }
    
    } catch (final CmdLineException cmdLineException) {
      System.err.println(cmdLineException.getMessage());
      parser.printUsage(System.err);
      System.exit(1);
    }*/


  }

  /**
   * Static method to print all publications in the array
   */
  public static void printAll() {

    for (Publication publication : IMMUTABLE_REPO) {
      System.out.println(publication.toString());
    }
  }

  private static class CommandLineOptions {

    @Option(name = "--type", aliases = "-t", metaVar = "<String>",
        usage = "Restricts the listed publications to the type(S) specified. "
            + "Accepted type identifiers are book, magazine and journal. "
            + "Multiple types may be specified by concatinating into a single string separated by commas, e.g book, magazine")
    private String pubTypesString;

    @Option(name = "--help", aliases = "-h", metaVar = "<boolean>",
        usage = "Prints out a help summary of available options and commands.")
    private boolean help;

    @Option(name = "--verbose", aliases = "-v", metaVar = "<boolean>",
        usage = "Provide extra explanatory information in the output about the actions the program is executing.")
    private boolean verbose;

    @Option(name = "--debug", aliases = "-d", metaVar = "<boolean>",
        usage = "Sets the log level to debug.")
    private boolean debug;

    @Option(name = "--info", aliases = "-i", metaVar = "<boolean>",
        usage = "Retrieves info on a particular book indicated by an acompanying isbn entered to the command line.")
    private boolean info;

    @Option(name = "--lookup", aliases = "-lu", depends = {"--info"}, metaVar = "<boolean>",
        usage = "Used to initiate an http query from Google Books.")
    private boolean lookUp;

    @Option(name = "--isbn", aliases = "isb", depends = {"--lookup"}, metaVar = "<String>",
        usage = "Used to look up a Book by its ISBN number. Must be accomponied by --lookup and --info"
            + "\nexample --info --lookup --isbn 12345")
    private String isbn;

    @Option(name = "--title", metaVar = "<String>",
        usage = "The title of the publication you wish to purchase or subscribe to")
    private String PubTitle;

    @Option(name = "--count", aliases = "-c", metaVar = "<int>",
        usage = "The number of Periodical issues to subscribe to. "
            + "\nexample: subscribe Nature 5 ")
    private int issueCount;

    @Argument(index = 0,
        usage = "Argument to identify whether to list<list> all publications,  "
            + "\nlist all publications by type <list --type (followed by a comma separated list of Publication types)>, \n"
            + "to purchase a Book <purchase --title {Title}> \n"
            + "or subscribe to a Periodical <subscribe --title {Title} --count {how many issues you wish to recieve}\n "
            + "\nexamples are: " + "\nlist (list all Publications)"
            + "\nlist --type(-t) journal,magazine(List all Journals and Magazines)"
            + "\npurchase --title \"The Hobbit\" (purchase a copy of this title)"
            + "\nsubscribe --title Nature --count(-c)5 (create a 5 issue subscription to Nature)\n")
    private String command;

    private boolean help() {
      return help;
    }

    private boolean debug() {
      return debug;
    }

    private boolean verbose() {
      return verbose;
    }

    private String getPubTitle() {
      return PubTitle;
    }

    private boolean info() {
      return info;
    }

    private boolean lookUp() {
      return lookUp;
    }

    private String getIsbn() {
      return isbn;
    }

    private String getCommand() {
      if (command == null) {
        return "command";
      } else {
        return command;
      }
    }

    public int issueCount() {
      return issueCount;
    }

    public String pubTypesString() {
      return pubTypesString;
    }

    public Set<PublicationType> getPubTypes() throws IOException {
      pubTypes = EnumSet.noneOf(PublicationType.class);
      for (final String singalPubTypeString : pubTypesString.split(",")) {
        pubTypes.add(PublicationType.valueOf(singalPubTypeString.toUpperCase()));
      }
      return pubTypes;
    }
  }
}
