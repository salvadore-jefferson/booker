/*
 * Copyright © 2015 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker;

import static com.jefferson.salvadore.booker.publication.Book.END_DATE_COMPARATOR;
import static com.jefferson.salvadore.booker.publication.PublicationFrequency.NA;
import static com.jefferson.salvadore.booker.publication.Range.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.jefferson.salvadore.booker.publication.Book;

/**
 * A test class for the {@link Book} {@link EndDateComparatorImpl} class
 * 
 * @author Salvadore Jefferson
 * @version 1.0.0 4-1-16
 *
 */
public class EndDateComparatorTest {

  Book book1 = new Book("A Tale of Two Cities", "Charles Dickens", of(0), 200_000_000, NA);
  Book book2 = new Book("The Lord of The Rings", "J.R.R Tolkien", of(1954, 1955), 150_000_000, NA);
  Book book3 = new Book("The Lord of The Rings", "J.R.R Tolkien", of(1954, 1955), 150_000_000, NA);

  @Test
  public void testEndDateFirstObjectSmaller() {
    final int result = END_DATE_COMPARATOR.compare(book1, book2);
    assertThat(result, is(lessThan(0)));
  }

  @Test
  public void testEndDateFirstObjectBigger() {
    final int result = END_DATE_COMPARATOR.compare(book2, book1);
    assertThat(result, is(greaterThan(0)));
  }

  @Test
  public void testEndDatesEqual() {
    final int result = END_DATE_COMPARATOR.compare(book2, book3);
    assertThat(result, is(equalTo(0)));
  }
}
