/*
 * Copyright © 2015 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker.repo;

import static com.jefferson.salvadore.booker.publication.PublicationFrequency.NA;
import static com.jefferson.salvadore.booker.publication.Range.of;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Test;

import com.jefferson.salvadore.booker.publication.Book;
import com.jefferson.salvadore.booker.publication.Journal;
import com.jefferson.salvadore.booker.repo.snapshot.SnapshotTopPublicationRepository;

public class SnapshotTopPublicationRepositoryTest {

  final PublicationRepository repo = new SnapshotTopPublicationRepository();

  @Test
  public void testFindPubByTitleMethod() throws NoSuchElementException, IOException {
    assertThat(repo.findByTitle("PLOS ONE").getTitle(), is("PLOS ONE"));

  }

  @Test
  public void testFindByTypeMethod() throws IOException {
    final List<Journal> journalList = repo.getByType(Journal.class);
    assertThat(journalList.size(), is(5));
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testAddNewPublicationMethod() throws NullPointerException, IOException {
    repo.addPub(new Book("Salvadore", "Jefferson", of(1887), NA));
    assertThat(repo.findByTitle("Salvadore").getTitle(), is("Salvadore"));

  }

  @Test(expected = UnsupportedOperationException.class)
  public void testRemovePublicationMethod() throws NullPointerException, IOException {
    repo.addPub(new Book("Salvadore", "Jefferson", of(1887), NA));
    assertThat(repo.findByTitle("Salvadore").getTitle(), is("Salvadore"));
    repo.removePubByTitle("Salvadore");
    repo.findByTitle("Salvadore");

  }
}
