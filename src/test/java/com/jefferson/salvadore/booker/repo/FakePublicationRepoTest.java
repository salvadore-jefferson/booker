/*
 * Copyright © 2016 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker.repo;

import static com.jefferson.salvadore.booker.publication.PublicationFrequency.NA;
import static com.jefferson.salvadore.booker.publication.Range.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jefferson.salvadore.booker.publication.Book;

public class FakePublicationRepoTest {

  private PublicationRepository fakeRepo;
  private Book testBook;

  @Before
  public void setUp() {
    fakeRepo = new FakePublicationRepository();
    testBook = new Book("The Hobbit", "J.R.R Tolkien", of(1937), 140_600_000, NA);
  }

  @Test
  public void testGetRepoSize() {
    assertThat(fakeRepo.getRepoSize(), is(equalTo(0)));
  }

  @Test
  public void testSetAndGetInventory() throws IOException {
    assertThat(fakeRepo.getInventoryCount(testBook.getTitle()), is(equalTo(0)));
    fakeRepo.addPub(testBook);
    fakeRepo.setInventoryCount(testBook.getTitle(), 1);
    assertThat(fakeRepo.getInventoryCount(testBook.getTitle()), is(equalTo(1)));
  }

  @Test
  public void testRemovePubByTitle() throws IOException {
    fakeRepo.addPub(testBook);
    fakeRepo.setInventoryCount(testBook.getTitle(), 10);
    assertThat(fakeRepo.getInventoryCount(testBook.getTitle()), is(equalTo(10)));
    fakeRepo.removePubByTitle(testBook.getTitle());
    assertThat(fakeRepo.getRepoSize(), is(equalTo(0)));
    assertThat(fakeRepo.getInventoryCount(testBook.getTitle()), is(equalTo(0)));

  }

  @After
  public void cleanUp() {
    fakeRepo = null;
    testBook = null;
  }
}
