/*
 * Copyright © 2016 Salvadore Jefferson
 */
package com.jefferson.salvadore.booker;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.jefferson.salvadore.booker.publication.Book;

/**
 * A test class for {@link JasonParser}
 * 
 * @author Salvadore Jefferson
 *
 */
public class JsonParserTest {

  private static JsonNode ROOT_NODE;
  private static Path PATH_TO_FILE;
  private static Book BOOK;

  @BeforeClass
  public static void setUp() throws IOException {
    PATH_TO_FILE = Paths.get("../booker/src/test/resources/Book.json");
    ROOT_NODE = JsonParser.getRootNode(PATH_TO_FILE);
  }

  @Test
  public void testGetRootNode() {
    assertThat(ROOT_NODE.path("kind").textValue(), is(equalTo("books#volumes")));
  }

  @Test
  public void testFindPublisher() {
    final JsonNode volumeInfo = ROOT_NODE.findValue("volumeInfo");
    assertThat(volumeInfo.path("publisher").textValue(),
        is(equalTo("Book Publishing Company (TN)")));
  }

  @Test
  public void testGettingSecondIndexFromArray() {
    final JsonNode volumeInfo = ROOT_NODE.findValue("volumeInfo");
    assertThat(volumeInfo.path("industryIdentifiers").get(1).path("identifier").textValue(),
        is(equalTo("9781570673313")));
  }

  @Test
  public void testCreatingBookFromJson() throws IOException {
    final JsonNode volumeInfo = ROOT_NODE.findValue("volumeInfo");
    BOOK = JsonParser.parseJson(PATH_TO_FILE);

    assertThat(BOOK.getTitle(), is(equalTo(volumeInfo.path("title").textValue())));
    assertThat(BOOK.getTitle(), is(equalTo("Gluten-Free Tips and Tricks for Vegans")));

    assertThat(BOOK.getAuthor(), is(equalTo(volumeInfo.path("authors").get(0).textValue())));
    assertThat(BOOK.getAuthor(), is(equalTo("Jo Stepaniak")));
  }

  @AfterClass
  public static void cleanUp() {
    PATH_TO_FILE = null;
    ROOT_NODE = null;
  }
}
